package test;

import main.ZaehlerDrucker;
import student.TestCase;

public class ZaehlerDruckerTest extends TestCase {

	/**Testet den Output am Ende des Programmes.
	 * @throws InterruptedException
	 */
	public void testZaehlerDrucker() throws InterruptedException {
		ZaehlerDrucker.main(new String[] { "0", "20" });
		assertFuzzyEquals("0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 ", systemOut().getHistory());
	}

	public void testWarteZeit(){
		int actual = ZaehlerDrucker.calcThreadSleepTime("20", "0", 300);
		assertEquals(9000, actual, 500);
	}
	

	
}
