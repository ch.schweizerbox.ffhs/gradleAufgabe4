package main;

//TODO Entfernen Sie den abstract modifier und implementieren Sie die fehlenden Methoden!
public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;

	@Override
	public int getWert() throws InterruptedException {
		return wert;
	}

	@Override
	public void setWert(int wert) throws InterruptedException {
		this.wert = wert;
	}

	@Override
	public boolean isHatWert() {
		return hatWert;
	}

	public void setisHatWert(boolean status) {
		hatWert = status;
	}

}
