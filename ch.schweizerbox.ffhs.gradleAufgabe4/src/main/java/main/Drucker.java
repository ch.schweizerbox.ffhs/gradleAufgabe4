package main;

public class Drucker extends Thread {
	private Speicher speicher;
	private int max;

	/**
	 * @param s
	 *            Das Speicherobjekt
	 * @param max
	 *            Das Maximum der Aufz�hlung
	 */
	Drucker(Speicher s, int max) {
		this.speicher = s;
		this.max = max;
	}

	/**
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen
	 * Leerzeichen.
	 * 
	 */
	@Override
	public void run() {
		// System.out.println("Drucker l�uft....");

		while (!Thread.currentThread().isInterrupted()) {
			try {
				synchronized (speicher) {
					while (speicher.getWert() < max) {

						while (!speicher.isHatWert()) {
							// System.out.println("drucker wartet...");
							speicher.wait();
						}

						System.out.print(speicher.getWert() + " ");
						speicher.setisHatWert(false);
						speicher.notifyAll();

					}
				}
				// System.out.println("\n....Drucker Ende!");
				Thread.currentThread().interrupt();

			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
	}

}
