package main;

/**
 * 
 * Der Aufruf ben�tigt zwei Parameter min und max - der Zaehler beginnt bei min
 * zu zaehlen und terminiert bei max.
 * 
 */

public class ZaehlerDrucker {

	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Usage: ZaehlerDrucker <min> <max>");
			System.exit(1);
		}

		Speicher s = new Speicher();
		Drucker d = new Drucker(s, Integer.parseInt(args[1]));
		int sleeptime = 300;
		Zaehler z = new Zaehler(s, Integer.parseInt(args[0]), Integer.parseInt(args[1]), sleeptime);

		z.start();
		d.start();

		// Wie lange muss ich warten?
		int wartezeit = calcThreadSleepTime(args[1], args[0], sleeptime);
		// int elemente = Integer.parseInt(args[1]) - Integer.parseInt(args[0]);
		// int wartezeitMitReserve = elemente * sleeptime + 3000;
		Thread.sleep(wartezeit);

	}

	/**
	 * @param arg1
	 *            maximum
	 * @param arg2
	 *            minimum
	 * @param sleeptime
	 *            gew�nschte Sleepdauer zwischen output
	 * @return ben�tigte Sleepdauer des MainThread
	 */
	public static int calcThreadSleepTime(String arg1, String arg2, int sleeptime) {
		int elemente = Integer.parseInt(arg1) - Integer.parseInt(arg2);
		int wartezeitMitReserve = elemente * sleeptime + 3000;

		return wartezeitMitReserve;
	}

}
