package main;

public class Zaehler extends Thread {

	private Speicher speicher;
	private int max, min;
	private int sleeptime;

	/**
	 * @param s
	 *            Speicherobjekt
	 * @param min
	 *            Das Minimum der Aufz�hlung
	 * @param max
	 *            Das Maximum der Aufz�hlung
	 * @param sleeptime
	 *            Zeit zwischen der Ausgabe der Zahlen - macht es gleich viel
	 *            anschaulicher :-)
	 */
	Zaehler(Speicher s, int min, int max, int sleeptime) {
		this.speicher = s;
		this.max = max;
		this.min = min;
		this.sleeptime = sleeptime;
	}

	/**
	 * Diese Run Methode z�hlt den Wert in Speicher hoch - von min bis max
	 * (einschliesslich).
	 * 
	 */
	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {
			try {
				synchronized (speicher) {
					// System.out.println("Z�hler l�uft....");
					speicher.setWert(min);
					speicher.setisHatWert(true);

					while (min < max) {

						while (speicher.isHatWert()) {
							// System.out.println("zahler wartet...");
							speicher.wait();
						}

						speicher.setWert(++min);
						speicher.setisHatWert(true);
						speicher.notifyAll();
						Thread.sleep(sleeptime);
					}

				}
				// System.out.println("\n....Z�hler Ende!");
				Thread.currentThread().interrupt();

			} catch (InterruptedException e1) {
				e1.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}

	}

}
